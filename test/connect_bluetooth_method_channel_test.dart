import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:connect_bluetooth/connect_bluetooth_method_channel.dart';

void main() {
  MethodChannelConnectBluetooth platform = MethodChannelConnectBluetooth();
  const MethodChannel channel = MethodChannel('connect_bluetooth');

  TestWidgetsFlutterBinding.ensureInitialized();

  setUp(() {
    channel.setMockMethodCallHandler((MethodCall methodCall) async {
      return '42';
    });
  });

  tearDown(() {
    channel.setMockMethodCallHandler(null);
  });

  test('getPlatformVersion', () async {
    expect(await platform.getPlatformVersion(), '42');
  });
}
