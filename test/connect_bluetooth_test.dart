import 'package:flutter_test/flutter_test.dart';
import 'package:connect_bluetooth/connect_bluetooth.dart';
import 'package:connect_bluetooth/connect_bluetooth_platform_interface.dart';
import 'package:connect_bluetooth/connect_bluetooth_method_channel.dart';
import 'package:plugin_platform_interface/plugin_platform_interface.dart';

class MockConnectBluetoothPlatform 
    with MockPlatformInterfaceMixin
    implements ConnectBluetoothPlatform {

  @override
  Future<String?> getPlatformVersion() => Future.value('42');
}

void main() {
  final ConnectBluetoothPlatform initialPlatform = ConnectBluetoothPlatform.instance;

  test('$MethodChannelConnectBluetooth is the default instance', () {
    expect(initialPlatform, isInstanceOf<MethodChannelConnectBluetooth>());
  });

  test('getPlatformVersion', () async {
    ConnectBluetooth connectBluetoothPlugin = ConnectBluetooth();
    MockConnectBluetoothPlatform fakePlatform = MockConnectBluetoothPlatform();
    ConnectBluetoothPlatform.instance = fakePlatform;
  
    expect(await connectBluetoothPlugin.getPlatformVersion(), '42');
  });
}
