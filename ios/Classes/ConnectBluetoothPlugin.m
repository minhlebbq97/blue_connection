#import "ConnectBluetoothPlugin.h"
#if __has_include(<connect_bluetooth/connect_bluetooth-Swift.h>)
#import <connect_bluetooth/connect_bluetooth-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "connect_bluetooth-Swift.h"
#endif

@implementation ConnectBluetoothPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftConnectBluetoothPlugin registerWithRegistrar:registrar];
}
@end
