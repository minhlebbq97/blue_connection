import Flutter
import UIKit
import CoreBluetooth

public class SwiftConnectBluetoothPlugin: NSObject, FlutterPlugin {
  public static func register(with registrar: FlutterPluginRegistrar) {
    let channel = FlutterMethodChannel(name: "connect_blue_channel", binaryMessenger: registrar.messenger())
    let instance = SwiftConnectBluetoothPlugin()
    registrar.addMethodCallDelegate(instance, channel: channel)
  }

    public var peripherals: [CBPeripheral]  = []
    public var resultConnectBlue: String = ""
    public let services : [CBUUID] = [CBUUID(string: "0000ffe0-0000-1000-8000-00805f9b34fb")]
    public let characteristicUUID : CBUUID = CBUUID(string: "0000ffe1-0000-1000-8000-00805f9b34fb")
    var ready: Bool = false
    var  result: FlutterResult? = nil
    var willCallBackFlutterResult : Bool = false
    var centralManager: CBCentralManager?
    var work: DispatchWorkItem?

  public func handle(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
    switch(call.method){
        case "getPlatformVersion":
            result("iOS " + UIDevice.current.systemVersion)
        case "bleScan":
        // Cancel before handle
        self.work?.cancel()
        if (self.centralManager != nil){
            if (self.centralManager!.isScanning){
                self.centralManager!.stopScan()
                self.result?("")
            }
        }

        // Reset value
        self.result = result
        self.resultConnectBlue = ""
       
        self.centralManager = CBCentralManager(delegate: self, queue: nil)
        self.centralManager!.scanForPeripherals(withServices: self.services)
        self.willCallBackFlutterResult = false
        
        let uuid = call.arguments as! String?

        work = DispatchWorkItem(block: {
            if !self.willCallBackFlutterResult {
                self.centralManager!.stopScan()
                result("")
            }
        })

        DispatchQueue.main.asyncAfter(deadline: .now() + 60.0, execute: self.work!)

        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
            print("self.peripherals count \(self.peripherals.count)")
               if (!self.peripherals.isEmpty){
                    for  peripheral in self.peripherals {
                        if (peripheral.identifier.uuidString == uuid){
                            self.centralManager?.connect(peripheral)
                        }
                    }
               }
                   }
        default:
            result(nil)
    }
  }
}


   extension SwiftConnectBluetoothPlugin : CBCentralManagerDelegate, CBPeripheralDelegate{
       public func centralManager( _ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
           print("peripheral \(peripheral)")
           self.peripherals.append(peripheral)
     }
       public func centralManagerDidUpdateState( _ central: CBCentralManager) {
       switch central.state {
        case .unknown:
         print("central.state is .unknown")
        case .resetting:
         print("central.state is .resetting")
        case .unsupported:
         print("central.state is .unsupported")
        case .unauthorized:
         print("central.state is .unauthorized")
        case .poweredOff:
         print("central.state is .poweredOff")
        case .poweredOn:
           print("central.state is \(central.state )")
           central.scanForPeripherals(withServices: [CBUUID(string: "0000ffe0-0000-1000-8000-00805f9b34fb")])
           ready = true;
         print("central.state is .poweredOn")
           print("central.isScanning is \(central.isScanning )")
        default:
         fatalError()
       }
     }
       public func centralManager( _ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
           peripheral.delegate = self
           peripheral.discoverServices(self.services)
         print("Connected to "+peripheral.name!)
       }

       public func centralManager( _ central: CBCentralManager, didFailToConnect peripheral: CBPeripheral, error: Error?) {
         print(error!)
       }

       public func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
           guard let service = peripheral.services?.first(where: { $0.uuid == CBUUID(string: "0000ffe0-0000-1000-8000-00805f9b34fb") }) else {
               print("ERROR: didDiscoverServices, service NOT found\nerror = \(String(describing: error)), disconnecting")
               return
           }
           print("didDiscoverServices, service found")
           peripheral.discoverCharacteristics(nil, for: service)
       }

       public func peripheral(_ peripheral: CBPeripheral, didModifyServices invalidatedServices: [CBService]) {
           print("didModifyServices")
           // usually this method is called when Android application is terminated
           if invalidatedServices.first(where: { $0.uuid == CBUUID(string: "0000ffe0-0000-1000-8000-00805f9b34fb") }) != nil {
               print("disconnecting because peripheral removed the required service")
           }
       }

       public func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
         //  print("didDiscoverCharacteristics \(error == nil ? "OK" : "error: \(String(describing: error))")")
           if let charIndicate = service.characteristics?.first(where: { $0.uuid == self.characteristicUUID}) {
               peripheral.setNotifyValue(true, for: charIndicate)
               var parameter = NSInteger(170)
               let data = NSData(bytes: &parameter, length: 1)
               peripheral.writeValue(data as Data, for: charIndicate,type: CBCharacteristicWriteType.withResponse)
               peripheral.discoverDescriptors(for: charIndicate)
           } else {
            //   print("WARN: characteristic for indication not found")
           }
       }

       public func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
           guard error == nil else {
              // print("didUpdateValue error: \(String(describing: error))")
               return
           }

           let data = characteristic.value ?? Data()
              let stringValue = String(data: data, encoding: .utf8) ?? ""
              if characteristic.uuid == self.characteristicUUID {
                  self.resultConnectBlue.append(stringValue)
                  DispatchQueue.main.async {
                      self.resultConnectBlue = self.resultConnectBlue.replacingOccurrences(of: ": ", with: ":", options: .literal, range: nil)
                      // Check MKP:50-65-83-1D-FC-59 B:90 A:276 S:231 C:285 P:4
                      let componentsResult = self.resultConnectBlue.components(separatedBy: " ")
                      var isCompleteResult = false
                      for element in componentsResult{
                          if (element.starts(with: "P:")){
                              isCompleteResult = true
                              self.willCallBackFlutterResult = true
                              break
                          }
                      }
                      guard let _ = self.result else {
                          return
                      }
                      print("self.resultConnectBlue \(self.resultConnectBlue)")
                      if (isCompleteResult){
                          self.centralManager?.stopScan()
                          self.result!(self.resultConnectBlue)
                      }
                   }
              }
          //    print("didUpdateValue '\(stringValue)'")
       }

       public func peripheral(_ peripheral: CBPeripheral,
                       didWriteValueFor characteristic: CBCharacteristic,
                       error: Error?) {
           print("didWrite \(error == nil ? "OK" : "error: \(String(describing: error))")")
       }

       public func peripheral(_ peripheral: CBPeripheral,
                       didUpdateNotificationStateFor characteristic: CBCharacteristic,
                       error: Error?) {
           guard error == nil else {
           //    print("didUpdateNotificationState error\n\(String(describing: error))")
               return
           }

           if characteristic.uuid ==  CBUUID(string: "0000ffe3-0000-1000-8000-00805f9b34fb") {
              // let info = characteristic.isNotifying ? "Subscribed" : "Not subscribed"
           //    print("characteristic \(info)")
           }
       }

       public func peripheral(_ peripheral: CBPeripheral, didDiscoverDescriptorsFor characteristic: CBCharacteristic, error: Error?) {
            if error != nil {
          //     print("error didDiscoverDescriptors")
           } else {
               if characteristic.descriptors?.count != 0 {
                   for d in characteristic.descriptors! {
                       let desc = d as CBDescriptor?
                       print("funcName: didDiscoverDescriptorsForCharacteristic, logString: \(desc!.description)")
                       print("peripheral \(peripheral.readValue(for: desc!))")
                   }
               } else {
             //      print("fcharacteristic.descriptors is empty")
               }
           }
       }

       public func peripheral(_ peripheral: CBPeripheral, didDiscoverIncludedServicesFor service: CBService, error: Error?) {
             if error != nil {
          //     print("error diddiscoverinclude")
             }

         //  print("funcName: didDiscoverIncludedServicesForService, logString: service: \(service.description) has \(service.includedServices?.count) included services")


           for s in ((service.includedServices as [CBService]?)!) {
            //   print("funcName: didDiscoverIncludedServicesForService, logString: \(s.description)")
           }
       }

       public func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor descriptor: CBDescriptor, error: Error?) {
           // Get and print user description for a given characteristic
           if descriptor.uuid.uuidString == CBUUIDCharacteristicUserDescriptionString,
               let userDescription = descriptor.value as? String {
              // print("Characterstic \(descriptor.characteristic?.uuid.uuidString) is also known as \(userDescription) ")
           }
       }

       public func peripheral(peripheral: CBPeripheral!, didUpdateValueForCharacteristic characteristic: CBCharacteristic!, error: NSError!) {
          // print("Data \(characteristic.value)")
        }
   }
