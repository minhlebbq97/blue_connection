import 'package:plugin_platform_interface/plugin_platform_interface.dart';

import 'connect_bluetooth_method_channel.dart';

abstract class ConnectBluetoothPlatform extends PlatformInterface {
  /// Constructs a ConnectBluetoothPlatform.
  ConnectBluetoothPlatform() : super(token: _token);

  static final Object _token = Object();

  static ConnectBluetoothPlatform _instance = MethodChannelConnectBluetooth();

  /// The default instance of [ConnectBluetoothPlatform] to use.
  ///
  /// Defaults to [MethodChannelConnectBluetooth].
  static ConnectBluetoothPlatform get instance => _instance;

  /// Platform-specific implementations should set this with their own
  /// platform-specific class that extends [ConnectBluetoothPlatform] when
  /// they register themselves.
  static set instance(ConnectBluetoothPlatform instance) {
    PlatformInterface.verifyToken(instance, _token);
    _instance = instance;
  }

  Future<String?> getPlatformVersion() {
    throw UnimplementedError('platformVersion() has not been implemented.');
  }

  Future<String> getData(String uuid) {
    throw UnimplementedError('platformVersion() has not been implemented.');
  }
}
