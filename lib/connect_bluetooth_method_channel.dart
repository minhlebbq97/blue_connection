import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';

import 'connect_bluetooth_platform_interface.dart';

/// An implementation of [ConnectBluetoothPlatform] that uses method channels.
class MethodChannelConnectBluetooth extends ConnectBluetoothPlatform {
  /// The method channel used to interact with the native platform.
  @visibleForTesting
  final methodChannel = const MethodChannel('connect_blue_channel');

  @override
  Future<String?> getPlatformVersion() async {
    final version = await methodChannel.invokeMethod<String>('getPlatformVersion');
    return version;
  }

  @override
  Future<String> getData(String uuid) async {
    final result = await methodChannel.invokeMethod<String>('bleScan', uuid);
    return result ?? '';
  }
}
