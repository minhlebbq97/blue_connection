import 'dart:async';

import 'package:flutter/services.dart';

class ConnectBlueCallNative {
  static const MethodChannel _channel = MethodChannel('connect_blue_channel');

  static Future<String?> get platformVersion async {
    final String? version = await _channel.invokeMethod('get_result');
    return version;
  }
}
