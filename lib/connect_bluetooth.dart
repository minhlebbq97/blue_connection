import 'connect_bluetooth_platform_interface.dart';

class ConnectBluetooth {
  Future<String?> getPlatformVersion() {
    return ConnectBluetoothPlatform.instance.getPlatformVersion();
  }

  Future<String> getData(String uuid) {
    return ConnectBluetoothPlatform.instance.getData(uuid);
  }
}
